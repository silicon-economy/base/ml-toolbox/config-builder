# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Just a simple ConfigBuilder caller, intended as a playground
"""

import logging
import os
from pathlib import Path
from sys import stdout

from config_builder import BaseConfigClass, ConfigBuilder


def main() -> None:
    project_root_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve().parent

    # Some very basic logging
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(levelname)-7.7s: "
        "[%(name)-30.30s]"
        "[%(threadName)-11.11s]"
        "[%(funcName)s():%(lineno)s] "
        "%(message)s",
        handlers=[logging.StreamHandler(stream=stdout)],
    )
    logger = logging.getLogger(__name__)
    # Set a different log level for the config-builder to demonstrate how this may be achieved
    logging.getLogger("config_builder").setLevel(logging.DEBUG)

    # A simple demonstration of a map to replace string in config items with their respective replacements
    replacements = {"ATTRIBUTE_1": "REPLACEMENT_1", "ATTRIBUTE_2": "REPLACEMENT_2"}

    # Construct a config builder to generate an empty base config
    config_builder = ConfigBuilder(
        class_type=BaseConfigClass,
        string_replacement_map=replacements,
        yaml_config_path=os.path.join(
            project_root_dir, "demo/simple_user_app_config.yaml"
        ),
    )
    config_builder.configuration.recursive_string_replacement(replacements)
    logger.info("Built %s", config_builder.configuration)


if __name__ == "__main__":
    main()
