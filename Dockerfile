FROM python:3.10

# Install poetry
ENV PIP_NO_CACHE_DIR=1
ENV POETRY_VERSION="1.5.1"
RUN curl -sSL https://install.python-poetry.org | python3 - --version "$POETRY_VERSION"
ENV PATH "/root/.local/bin:$PATH"
RUN poetry --version

COPY scripts/ "/scripts"
RUN chmod -R ugo+rx /scripts/

# Define paths. Let all path definitions always end with a '/'!

# Path to all code we will place in this container image
ENV BUILD_ENV_DIR="/build-env/"
# Path to the actual project
ENV PROJECT_DIR="${BUILD_ENV_DIR}/config-builder/"
# Create those locations
RUN mkdir -p "$PROJECT_DIR"

COPY pyproject.toml "$PROJECT_DIR"
COPY poetry.lock "$PROJECT_DIR"

WORKDIR "$PROJECT_DIR"

ENV VIRTUAL_ENV="${BUILD_ENV_DIR}venv"
ENV PATH="${VIRTUAL_ENV}bin:$PATH"
RUN python3 -m venv "$VIRTUAL_ENV" \
  && poetry run pip install --upgrade pip \
  && poetry install --no-interaction --no-ansi --no-root --all-extras

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

# ====================================================================
# Label the image
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="Fraunhofer IML - ConfigBuilder - Main gitlab-runner container" \
      org.opencontainers.image.description="Fraunhofer IML - ConfigBuilder - Main gitlab-runner container"
