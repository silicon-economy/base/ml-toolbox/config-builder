[[section-runtime-view]]

== Runtime View

In contrast to the <<Building Block View, static building block view>>, this section visualizes dynamic aspects of the ConfigBuilder. The runtime view describes concrete behavior and interactions of the system’s building blocks across the architecture, i.e., how instances of building blocks of the ConfigBuilder perform their job and communicate at runtime.

=== Base API Operations

In the following the most important classes and their methods of the ConfigBuilder are described.


==== BaseConfigClass - recursive_check_mutuality_and_update_replacements

As stated before, the BaseConfigClass should be used as superclass of any configuration class that
has to be built with the ConfigBuilder. It provides a recursive function, that is executed on the
class tree that is build utilizing the "ChildField" of the related framework. For any child class
inherits from the BaseConfigClass these recursive function "recursive_check_mutuality_and_update_replacements" is called.
During the recursion two features are executed:

- The initial call of the "recursive_check_mutuality_and_update_replacements" method provides a "string_replacement_map".
The keys of this map can be used later on to replace placeholders in string attributes
(see: <<BaseConfigClass - recursive_string_replacement>>). These keys are checked against any
attribute name of the class tree. If the name of configuration attribute matches a key of the string_replacement_map,
the value of the string_replacement_map is updated accordingly.
- It is possible to define attributes at one level of the configuration class that are not valid
to be present together at runtime. They are mutually exclusive. To use this feature, configuration classes have to implement the _mutual_attributes function of the BaseConfigClass.

==== BaseConfigClass - recursive_check_values

Analog to the "recursive_check_mutuality_and_update_replacements" method the "recursive_check_values"
recursively calls the "check_values(...)" for any attribute that inherits from the "BaseConfigClass".
It is intended to be used for defining value checks like ranges for numbers of if a certain path
does exist.

==== BaseConfigClass - recursive_string_replacement

The "recursive_string_replacement" follows the same principle as "recursive_check_mutuality_and_update_replacements".
However here the actual replacement in string attributes takes place. Together with the information
that is provided by the string_replacement_map, any attribute of type string (string itself, list
of strings, string in a dictionary) is checked for placeholders (keys of the string_replacement_map)
that have to be replaced by the according values of the string_replacement_map.


==== YAML Constructors

The yaml_constructors contains additional constructors that can be added to the yaml package. In the following the functionality of these constructors is described.

===== join_string

Provides the functionality to concatenate all given strings:

------
yaml_entry: !join_string ["string_1", "string_1"]  => string_1string_2
------

===== join_string_with_delimiter

Same as join_string, but the strings are concatenated with a delimiter in between (e.g. ";" or "_"):

------
yaml_entry: !join_string_with_delimiter ["_", "string_1", "string_1"]  => string_1_string_2
------

===== join_path

Provides the functionality to build a file path by utilizing os.path.join. Every given element is joined to a file path:

------
yaml_entry: !join_string_with_delimiter ["home", test", "dev"]  => home/test/dev/
------

===== join_object

Provides the functionality to include the respective attribute to the value that is given by the attribute of another yaml configuration file. It is allowed to path as many yaml files as you want after the first parameter. In case of multiple yaml files, the attribute to which the output of the !join_object is passed should be an attribute of type "list".

------
# PATH_TO_YAML/yaml_config_1.yaml
attribute: "test"


# PATH_TO_YAML/yaml_config_2.yaml
attribute: !join_object ["",  "PATH_TO_YAML/yaml_config_1.yaml", "..."]
------

===== join_object_from_config_dir

Allows to include the content of another yaml file that is searched in different directories.

------
# PATH_TO_YAML/yaml_config.yaml
attribute: "test"


# PATH_TO_YAML/yaml_config_2.yaml
attribute: !join_object [
  "",
  "yaml_config.yaml"
  "PATH_TO_DIRECTROY",

  ... more directories to search for "yaml_config.yaml"
]
------