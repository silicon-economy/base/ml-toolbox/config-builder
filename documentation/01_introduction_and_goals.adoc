[[section-introduction]]

== Introduction

=== Silicon Economy

****
In a Silicon Economy, services are executed and used via platforms.
A Silicon Economy Platform consists of five different kinds of major components, namely one or more SE Services, one or more IDS connectors, a Logistics Broker, an optional IoT Broker and an optional Blockchain Full-Node.
This platform is the environment of SE Services, its location does not matter (e.g., IT environment of a company, a cloud environment, etc.).

SE Services are professional (logistics) services, consisting of IT and/or physical services.
They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process.

.SE Big Picture
image::images/01_se_big_picture.png[SE Big Picture]

Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored.
SE Services are documented.
SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use.
Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.
****


=== Machine Learning Toolbox as a Silicon Economy Platform Component
Artificial Intelligence and especially Machine Learning (ML) are important technologies for SE Services. They allow to map more complex interrelationships than traditional methods and make use of patterns in data. The ML Toolbox supports the development of ML software by providing tools for frequently occurring tasks in the development process of every ML application. It is structured by process steps of the project (data generation, preprocessing, modeling, postprocessing) as well as the area of ML (e.g., computer vision, natural language processing, time series prediction) the tool supports.

The following diagram shows the structure and components of the ML Toolbox:

.ML Toolbox Big Picture
image::images/01_ml_toolbox_big_picture.png[ML Toolbox Big Picture]

The ConfigBuilder is one of the first components to be added to the ML Toolbox.
It provides an SDK for building configuration classes on the basis of given content from YAML configuration files.
It is build on top of the related SDK. The key concept is as follows: To be able to parse the content of a YAML configuration file, the nested tree structure of the YAML has to be replicated by the according structure in python classes. More details about the architecture of the ConfigBuilder are given in chapter <<Building Block View, 5>>.

=== Requirements Overview

From an end users’ point of view, the ConfigBuilder component of the ML Toolbox handles configuration of development components (e.g. the models of the MLCVZoo).

The requirements for the ConfigBuilder as a component of the ML Toolbox are summarized in the following:

. What is the ConfigBuilder component of the ML Toolbox?
.. A tool within the ML Toolbox component of a Silicon Economy Platform to support the handling of configurations
.. An SDK to build python configuration classes

. Essential Features
.. Parse YAML configuration files and create configuration classes out of the parsed content.

. Functional requirements
.. The configuration classes have to be build utilizing the related framework.


=== Quality Goals

****
The following table describes the top two quality goals of the ConfigBuilder as a component of the ML Toolbox classified byISO/IEC 9126-1 characteristics.
The order of goals gives a rough idea of their importance.
****

TODO:

- change to yaml-config-builder goals

[cols="1e,2e",options="header"]
|===
|Quality Goal
|Motivation/Description

|Reliability (Robustness)
|The ConfigBuilder must be able to parse any given yaml file and build a configuration class that fits to this content.

|Usability (Simplicity)
|The ConfigBuilder must simplify the development that is needed to handle configuration classes.

|Usability (Maintainability)
|The ConfigBuilder must be easily maintainable.

|===

//TODO:
//
//- there are some quality goals missing in this list. Is this on purpose? => They do not apply for now
//  (Functionality (Secure updates), Usability (Attractiveness), Functionality (Completeness), Usability / Maintainability)

=== Stakeholders

[role="arc42help"]
****
The following table lists the most important stakeholders of the ConfigBuilder (person, roles and/or organizations) and their respective expectations, goals and intentions.
****

[cols="4",options="header"]
|===
|Role/Name
|Contact
|Expectations
|Example

|SE Service User
|An SE Service user wants to use a ML Toolbox component
|Wants to modify the behavior of the component by using a configuration file
|

|SE Service Owner
|Offers an SE Service that can be configured by well known standards
|Wants to ensure that the behavior of a ML Toolbox component can be controlled by configuration files
|

|Service Developer
|Develops a ML Toolbox component
|Needs to define configurations that are easy to manage and create

|
|===
