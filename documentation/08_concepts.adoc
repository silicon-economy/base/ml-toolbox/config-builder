[[section-concepts]]

== Cross-cutting Concepts

Concepts form the basis for conceptual integrity (consistency, homogeneity) of the architecture.
Thus, they are an important contribution to achieve inner qualities.
This section describes general structures and system-wide cross-cutting aspects.
It also presents various technical solutions.

=== ConfigBuilder Domain Model

The key concept that should be applied to any configuration class is the usage of the BaseConfigClass.
It provides the major features for ensuring that a recursive check / placeholder replacement can take place.

=== Cross-cutting Solutions

The following solutions are applied in all components of the ConfigBuilder.

==== Error Handling

Runtime errors within the ConfigBuilder should, wherever possible, lead to an immediate crash of the respective component whenever the error can not be resolved in a timely fashion by the component itself. Every error is reported via python logging.

==== Logging

We use standard python logging mechanism. Besides, the modules in the tools package, no logging is initialized per default. The logging handlers have to be managed by the application of the user, where the ConfigBuilder is used as an SDK.


==== Testability

In standard unit testing, which examines the individual classes, tests are named like the classes themselves with suffix "Test".
In addition, there are tests that examine the interaction of modules, and in extreme cases the whole system.
The standard SE testing guidelines apply.

NOTE: Some unit tests may be better characterized as "plausibility tests"
