[[section-tutorial]]

== Tutorial

In this chapter the main usage scenarios of the ConfigBuilder are described.

=== Dependency Installation

In this chapter the installation of the ConfigBuilder is described. It is stated how the ConfigBuilder can be setup up to start developing and how it can be added as dependency in your project

==== ConfigBuilder dev setup

In the ConfigBuilder https://github.com/python-poetry/poetry[poetry] is used to setup the python environment.

* install:

-------
poetry install
-------

==== ConfigBuilder as dependency

In the following you find two examples how the ConfigBuilder can be added as dependency in your project:

* poetry (pyproject.toml)

-------
yaml-config-builder = { git = "https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/config-builder.git", rev = "YOUR_GIT_REFERENCE"}
-------

* pip (requirements.txt)

-------
# pypi
yaml-config-builder

# pip+https:
git+https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/config-builder.git

# pip+ssh:
git+ssh://git@git.openlogisticsfoundation.org:silicon-economy/base/ml-toolbox/config-builder.git
-------

=== Example: Build and use a configuration class

-------
# ========================================
# Example config class

@related.mutable(strict=True)
class GlobalConfig(BaseConfigClass):
    EXAMPLE: str = related.StringField(required=False, default="")
    EXAMPLE_REPLACEMENT: str = related.StringField(
        required=False, default="EXAMPLE_STRING"
    )
    EXAMPLE_LIST: List[str] = related.SequenceField(cls=str, required=False)

# ========================================
# Example config file: test_config_1.yaml

EXAMPLE_LIST:
 - "HELLO"
 - "WORLD"

# ========================================
# Example config file: test_config_2.yaml

EXAMPLE: "TEST_BUILD_CONFIG"

EXAMPLE_REPLACEMENT: "EXAMPLE"

EXAMPLE_LIST: !join_object ["EXAMPLE_LIST", "test_config_1.yaml"]

# ========================================
# Example code for using the ConfigBuilder

string_replacement_map: Dict[str, str] = {
    "HELLO": "",
    "WORLD": "EARTH"
    "EXAMPLE": "",
}

os.environ["HELLO"] = "HEY"

# REMARK:
# - The key "EXAMPLE" of the string_replacement_map matches the name of a configuration attribute
#   of the GlobalConfig class. Therefore the value of this configuration attribute will be used
#   to replace any placholders
# - Set the environment value "HELLO" in order to replace the value in the configuration attribute
# - The value of the placeholder "WORLD" will be used directly

# Create the ConfigBuilder instance to build the configuration for the YAML configuration file
# "test_config_2.yaml" and use the string_replacement_map for replacing placeholders.
config_builder = ConfigBuilder(
    class_type=GlobalConfig,
    yaml_config_path="test_config_2.yaml",
    string_replacement_map=string_replacement_map,
)


# After the call of the ConfigBuilder, the config_builder.base_config contains the following:

config_builder.configuration.EXAMPLE: "TEST_BUILD_CONFIG"
config_builder.configuration.EXAMPLE_REPLACEMENT: "TEST_BUILD_CONFIG"
config_builder.configuration.EXAMPLE_LIST:
 - "HEY"
 - "EARTH"


# Detailed explaination:
# - Since the value of the attribute GlobalConfig.EXAMPLE_REPLACEMENT="EXAMPLE", it is
#   set to the same value as GlobalConfig.EXAMPLE, since the string "EXAMPLE" is replaced
#   by the equivalent plachoder value
# - The entries of the list GlobalConfig.EXAMPLE_LIST are replaced by the placholder values
#   as explained before


# Furthermore, you can build a configuraiton directly from a configuration dictionary:

configuration = ConfigBuilder.build_configuration_from_dict(
    class_type=GlobalConfig,
    config_dict={
        "EXAMPLE": "TEST_BUILD_CONFIG"
        "EXAMPLE_REPLACEMENT": "EXAMPLE"
        "EXAMPLE_LIST": ["HELLO", "WORLD"]

    },
)

-------


More examples can be found in the unit tests in the *tests/* directory.
