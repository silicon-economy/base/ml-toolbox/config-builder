#!/bin/sh

GENERATED_LICENSES_FILE="${GENERATED_LICENSES_FILE:=third-party-licenses/third-party-licenses.csv}"
poetry run pip3 uninstall -y yaml-config-builder
poetry install --no-root --only main --sync "$@"
poetry run pip3 install pip-licenses
poetry run pip-licenses --ignore-packages pkg-resources PyGObject chardet dbus-python ssh-import-id distro --with-urls --format=csv --output-file="$GENERATED_LICENSES_FILE"
