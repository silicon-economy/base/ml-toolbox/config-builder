# Config Builder

The ConfigBuilder provides an SDK for building configuration classes on the basis of 
given content from YAML configuration files. Details about the ConfigBuilder can be
found in the [documentation](documentation/index.adoc).

## Install

The installation and setup of the ConfigBuilder is described in [chapter 11](documentation/12_tutorial.adoc) 
of the documentation.

# Technology stack

- Python 

## License
See the license file in the top directory.

## Contact information


Maintainer: 
- Maximilian Otten <a href="mailto:maximilian.otten@iml.fraunhofer.de?">maximilian.otten@iml.fraunhofer.de</a>

Development Team: 
- Christian Hoppe <a href="mailto:christian.hoppe@iml.fraunhofer.de?">christian.hoppe@iml.fraunhofer.de</a>
- Oliver Bredtmann <a href="mailto:oliver.bredtmann@dbschenker.com?">oliver.bredtmann@dbschenker.com</a>
- Thilo Bauer <a href="mailto:thilo.bauer@dbschenker.com?">thilo.bauer@dbschenker.com</a>


