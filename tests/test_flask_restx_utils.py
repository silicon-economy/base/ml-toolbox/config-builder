# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Test the flask_restx_utils."""

import json
import os
from typing import Any, Dict, List, Optional, Tuple
from unittest import mock

import pytest
from attr import define
from flask import Blueprint, Flask
from flask_restx import Api, Namespace, Resource
from related import (
    BooleanField,
    ChildField,
    FloatField,
    IntegerField,
    SequenceField,
    StringField,
)

from config_builder import BaseConfigClass


def test_flask_module_not_found() -> None:
    """
    Test if the error handling is correct when the optional module flask_restx is not installed.
    This error has to be teste before the other tests, since otherwise the patch of the flask_restx
    module does not work.
    """

    @define
    class TestConfig:
        """

        Attributes:
            test_int: Test integer attribute
                      with line break.
        """

        __related_strict__ = True

        test_int: int = IntegerField(default=0)

    # create the api endpoint
    api_bp = Blueprint("api", __name__)
    local_test_api = Api(api_bp, version="0.1.0", title="Test API")

    test_namespace_api = Namespace("test", description="Test endpoints")
    local_test_api.add_namespace(test_namespace_api)

    with mock.patch.dict("sys.modules", {"flask_restx": None}):
        import config_builder.flask_restx_utils

        recursive_resolve_fields = (
            config_builder.flask_restx_utils.recursive_resolve_fields
        )

    with pytest.raises(
        ValueError,
        match="flask_restx is not installed. In order to use the feature, install the "
        "config-builder with the extra 'flask-restx-utils'",
    ):
        recursive_resolve_fields(api=test_namespace_api, attr_class=TestConfig)

    assert config_builder.flask_restx_utils.flask_restx is None
    assert config_builder.flask_restx_utils.Namespace is None
    assert config_builder.flask_restx_utils.Model is None
    assert config_builder.flask_restx_utils.FlaskRestXList is None
    assert config_builder.flask_restx_utils.FlaskRestXNested is None
    assert config_builder.flask_restx_utils.FlaskRestXRaw is None
    assert config_builder.flask_restx_utils.FlaskRestXInteger is None
    assert config_builder.flask_restx_utils.FlaskRestXFloat is None
    assert config_builder.flask_restx_utils.FlaskRestXString is None
    assert config_builder.flask_restx_utils.FlaskRestXBoolean is None


def test_correct_schema(project_root) -> None:
    from config_builder.flask_restx_utils import recursive_resolve_fields

    # create the api endpoint
    api_bp = Blueprint("api", __name__)
    local_test_api = Api(api_bp, version="0.1.0", title="Test API")

    test_namespace_api = Namespace("test", description="Test endpoints")
    local_test_api.add_namespace(test_namespace_api)

    app = Flask("TestApp")
    app.register_blueprint(api_bp)

    @define
    class TestSubConfig(BaseConfigClass):
        test_attribute: int = IntegerField()

    class TestSubConfigNotAttr:
        test_attribute: int = IntegerField()

    @define
    class TestConfig:
        """

        Attributes:
            test_int: Test integer attribute
                      with line break.
            test_string: Test string attribute.
            test_bool: Test boolean attribute.
            test_float: Test float attribute.
            test_dict: Test dictionary attribute.
            test_dict_optional: Test optional dictionary attribute.
            test_list: Test list attribute.
            test_list_optional: Test optional list attribute.
        """

        __related_strict__ = True

        _protected_test_list: List[TestSubConfig] = SequenceField(cls=TestSubConfig)

        __private_test_list: List[TestSubConfig] = SequenceField(cls=TestSubConfig)

        test_list: List[TestSubConfig] = SequenceField(cls=TestSubConfig)

        test_list_no_attr: List[TestSubConfigNotAttr] = SequenceField(
            cls=TestSubConfigNotAttr
        )

        test_int: int = IntegerField(default=0)

        test_optional_int: Optional[int] = ChildField(
            cls=Optional[int], default=None, required=False
        )

        test_string: str = StringField(default="")

        test_bool: bool = BooleanField(default=False)

        test_float: float = FloatField(default=False)

        test_dict: Dict[str, Any] = ChildField(cls=dict, default={}, required=False)

        test_dict_optional: Optional[Dict[str, Any]] = ChildField(
            cls=dict, default=None, required=False
        )

        test_list_optional: Optional[List[TestSubConfig]] = SequenceField(
            cls=TestSubConfig, default=None, required=False
        )

    @test_namespace_api.route("/")
    class Test(Resource):
        """Test api."""

        @test_namespace_api.doc("start")
        @test_namespace_api.response(code=200, description="Successfully tested.")
        @test_namespace_api.response(code=500, description="Error while testing.")
        @test_namespace_api.expect(
            recursive_resolve_fields(api=test_namespace_api, attr_class=TestConfig),
            validate=False,
        )
        def put(self) -> Tuple[str, int]:
            """Test API endpoint for the mlcvzoo-edge-service components.

            Returns:
                A tuple containing the response message and the status code.
            """

            return "Successful Test", 200

    with app.test_request_context():
        app_doc = local_test_api.__schema__

    # Comment this in if you need to generate a new test_api_schema.json:
    #
    generated_test_api_path = os.path.join(
        project_root, "test_output/test_api/test_api_schema.json"
    )

    os.makedirs(os.path.dirname(generated_test_api_path), exist_ok=True)
    with open(generated_test_api_path, "w") as generated_test_api_file:
        json.dump(app_doc, generated_test_api_file, indent=2)

    expected_test_api_path = os.path.join(
        project_root, "tests/data/expected_test_api_schema.json"
    )
    with open(expected_test_api_path, "r") as expected_test_api_file:
        expected_test_api_schema = json.load(expected_test_api_file)

    assert app_doc == expected_test_api_schema
