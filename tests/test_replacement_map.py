# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
import json
import os

from config_builder.replacement_map import (
    clear_replacement_map,
    clear_replacement_map_values,
    get_current_replacement_map,
    get_replacement_map_keys,
    get_replacement_value,
    set_replacement_map_value,
)
from config_builder.utils import replace_with_os_env


def test_get_replacement_map_keys():
    clear_replacement_map()

    replacement_map = {
        "PLACEHOLDER": "PLACEHOLDER_VALUE",
        "PLACEHOLDER_2": "PLACEHOLDER_VALUE_2",
    }

    for key, value in replacement_map.items():
        set_replacement_map_value(key=key, value=value)

    assert get_replacement_map_keys() == ["PLACEHOLDER", "PLACEHOLDER_2"]


def test_clear_replacement_map_values():
    clear_replacement_map()

    replacement_map = {
        "PLACEHOLDER": "PLACEHOLDER_VALUE",
        "PLACEHOLDER_2": "PLACEHOLDER_VALUE_2",
    }

    expected_replacement_map = {"PLACEHOLDER": "", "PLACEHOLDER_2": ""}

    for key, value in replacement_map.items():
        set_replacement_map_value(key=key, value=value)

    clear_replacement_map_values()

    assert get_current_replacement_map() == expected_replacement_map


def test_reset_replacement_map_value():
    clear_replacement_map()

    replacement_map = {
        "PLACEHOLDER": "PLACEHOLDER_VALUE",
        "PLACEHOLDER_2": "PLACEHOLDER_VALUE_2",
    }

    expected_replacement_map = {
        "PLACEHOLDER": "",
        "PLACEHOLDER_2": "PLACEHOLDER_VALUE_2",
    }

    for key, value in replacement_map.items():
        set_replacement_map_value(key=key, value=value)

    set_replacement_map_value(key="PLACEHOLDER", value=None)

    assert get_current_replacement_map() == expected_replacement_map


def test_get_replacement_map_value():
    clear_replacement_map()

    replacement_map = {
        "PLACEHOLDER": "PLACEHOLDER_VALUE",
        "PLACEHOLDER_2": "PLACEHOLDER_VALUE_2",
    }

    for key, value in replacement_map.items():
        set_replacement_map_value(key=key, value=value)

    assert get_replacement_value(key="PLACEHOLDER") == "PLACEHOLDER_VALUE"
    assert get_replacement_value(key="PLACEHOLDER_3") is None


def test_replace_with_os_env():
    os_environ_copy = os.environ.copy()

    os.environ["TEST_VARIABLE"] = "HELLO"
    os.environ["TEST_VARIABLE_2"] = "WORLD"

    test_dict = {
        "test": "${TEST_VARIABLE}_${TEST_VARIABLE_2}",
        "test_2": "${NOT_DEFINED_VAR}",
    }

    for key, value in test_dict.items():
        test_dict[key] = replace_with_os_env(value=value)

    print(json.dumps(test_dict, indent=2))

    assert test_dict["test"] == "HELLO_WORLD"
    assert test_dict["test_2"] == "${NOT_DEFINED_VAR}"

    os.environ.clear()
    os.environ.update(os_environ_copy)
