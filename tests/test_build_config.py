# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import copy
import logging
import os
import shutil
import sys
import typing
from threading import Thread
from typing import Dict, List, Optional, Tuple
from unittest import TestCase

import pytest
import related
import yaml
from global_config import GlobalConfig, GlobalConfig2, GlobalConfigCheckMutual
from pytest import raises
from related.functions import to_model
from yaml import FullLoader

from config_builder import ConfigBuilder
from config_builder.replacement_map import (
    clear_replacement_map,
    get_current_replacement_map,
    set_replacement_map_value,
    update_replacement_map_from_os,
)

logger = logging.getLogger(__name__)


def get_project_root(search_depth: int = 1) -> Tuple[str, Dict[str, str]]:
    this_dir = os.path.dirname(os.path.abspath(__file__))
    setup_paths = [".." + os.path.sep] * search_depth
    setup_path = ""
    for s in setup_paths:
        setup_path += s

    project_dir = os.path.realpath(os.path.join(this_dir, setup_path))
    return project_dir, {"PROJECT_ROOT_DIR": project_dir}


class TestConfigBuilder(TestCase):
    def tearDown(self) -> None:
        project_root_dir, replacement_map = get_project_root()

        shutil.rmtree(os.path.join(project_root_dir, "test_output"), ignore_errors=True)

    def test_build_config_no_valid_parameters(self) -> None:
        with raises(ValueError) as ve:
            ConfigBuilder(
                class_type=GlobalConfig,
            )

            assert (
                str(ve) == "Cannot build a config. "
                "Both, the configuration object and yaml_config_path are None"
            )

    def test_build_config_base(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(project_root_dir, "tests/data/test_config_base.yaml")

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        assert configuration.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration.string_replacement_map == replacement_map

        configuration_2: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map={},
            ).configuration,
        )

        assert configuration_2.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration_2.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration_2.string_replacement_map == {}

        configuration_3 = related.to_model(GlobalConfig, configuration.to_dict())

        assert configuration_3.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration_3.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration_3.string_replacement_map == {}

    def test_build_config_from_dict(self) -> None:
        _, replacement_map = get_project_root()

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder.build_configuration_from_dict(
                config_dict={"EXAMPLE": "TEST_BUILD_CONFIG"},
                class_type=GlobalConfig,
                string_replacement_map=replacement_map,
            ),
        )

        logger.debug(configuration)

        assert configuration.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration.string_replacement_map == replacement_map

    def test_build_config_related_strict(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_base_conversion.yaml"
        )

        with raises(ValueError):
            ConfigBuilder(
                class_type=GlobalConfig2,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            )

    def test_build_config_related_strict_after_conversion(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_build_config_related_strict_after_conversion.yaml",
        )

        configuration: GlobalConfig2 = typing.cast(
            GlobalConfig2,
            ConfigBuilder(
                class_type=GlobalConfig2,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        config_dict = related.to_dict(configuration, suppress_private_attr=True)

        config_dict["test_strict"] = "HERE"

        with raises(ValueError):
            related.to_model(GlobalConfig2, config_dict)

    def test_build_config_base_to_yaml(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(project_root_dir, "tests/data/test_config_base.yaml")

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        test_output_dir = os.path.join(project_root_dir, "test_output")
        os.makedirs(test_output_dir, exist_ok=True)

        test_yaml_path = os.path.join(test_output_dir, "test_config_base_to_yaml.yaml")
        with open(test_yaml_path, "w") as yaml_file:
            configuration.to_yaml(
                yaml_package=yaml, dumper_cls=yaml.Dumper, stream=yaml_file
            )

        configuration_2: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=test_yaml_path,
                string_replacement_map={},
            ).configuration,
        )

        assert configuration_2.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration_2.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration_2.string_replacement_map == {}

    def test_build_config_base_to_json(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(project_root_dir, "tests/data/test_config_base.yaml")

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        test_output_dir = os.path.join(project_root_dir, "test_output")
        os.makedirs(test_output_dir, exist_ok=True)

        test_json_path = os.path.join(test_output_dir, "test_config_base_to_json.json")
        with open(test_json_path, "w") as json_file:
            json_file.write(configuration.to_json())

        with open(test_json_path, "r") as json_file:
            configuration_2: GlobalConfig = related.from_json(
                stream=json_file, cls=GlobalConfig
            )

        assert configuration_2.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration_2.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"
        assert configuration_2.string_replacement_map == {}

    def test_build_config_failed_check_in_base_class(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_base_failed_check_in_base_class.yaml",
        )

        with raises(ValueError) as exception:
            _ = ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

        logger.debug(exception)

    def test_build_config_failed_check_in_attribute(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_base_failed_check_in_attribute.yaml",
        )

        with raises(ValueError) as exception:
            _ = ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

        logger.debug(exception)

    def test_build_config_failed_check_in_sequence(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_base_failed_check_in_sequence.yaml",
        )

        with raises(ValueError) as ve:
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

            assert str(ve) == "Check for attribute 'sequence' failed!"

    def test_build_config_failed_check_in_sequence_nested(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_base_failed_check_in_sequence_nested.yaml",
        )

        with raises(ValueError) as ve:
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

            assert str(ve) == "Check for attribute 'nested_attribute' failed!"

    def test_build_config_join_path(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_join_path.yaml"
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        assert global_config.EXAMPLE_REPLACEMENT == os.path.join(
            "home", "test", "dev", "config-builder"
        )

    def test_build_config_join_string(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_join_string.yaml"
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        assert global_config.EXAMPLE_REPLACEMENT == "TEST_TEST2"

    def test_build_config_join_string_with_delimiter(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_string_with_delimiter.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        assert global_config.EXAMPLE_REPLACEMENT == "TEST:TEST2"

    def test_replace_string_in_dict(self) -> None:
        expected_dict = {
            "test": "TEST_BUILD_CONFIG",
            "test_list": [
                "TEST_BUILD_CONFIG",
                "TEST_BUILD_CONFIG",
                {
                    "test_list_item_dict": "TEST_BUILD_CONFIG",
                    "test_list_item_dict_dict": {
                        "test": "TEST_BUILD_CONFIG",
                        "test_list": [
                            "TEST_BUILD_CONFIG",
                            {"test_list_item_dict_dict_list_dict": "TEST_BUILD_CONFIG"},
                        ],
                    },
                },
            ],
            "test_dict": {
                "test": "TEST_BUILD_CONFIG",
                "test_list": ["TEST_BUILD_CONFIG", "TEST_BUILD_CONFIG"],
                "test_dict_dict": {
                    "test": "TEST_BUILD_CONFIG",
                    "test_list": ["TEST_BUILD_CONFIG", "TEST_BUILD_CONFIG"],
                },
            },
        }

        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_replace_string_in_dict.yaml"
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map={"EXAMPLE": ""},
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        logger.debug(global_config.string_replacement_map)
        assert global_config.EXAMPLE == global_config.dict_parameters["test"]
        assert global_config.dict_parameters == expected_dict

    def test_replace_string_in_dict_with_environment_variable(self) -> None:
        old_env = copy.deepcopy(os.environ)

        replacement_value = "os-environ-replacement"
        replacement_value_2 = "os-environ-replacement_content-2"

        os.environ["TEST_BUILD_CONFIG"] = replacement_value
        os.environ["TEST_BUILD_CONFIG_2"] = replacement_value_2
        os.environ["EXAMPLE"] = replacement_value

        expected_dict = {
            "test": replacement_value,
            "test_list": [
                f"{replacement_value}/{replacement_value_2}/test_path",
                replacement_value,
                {
                    "test_list_item_dict": replacement_value,
                    "test_list_item_dict_dict": {
                        "test": replacement_value,
                        "test_list": [
                            replacement_value,
                            {"test_list_item_dict_dict_list_dict": replacement_value},
                        ],
                    },
                },
            ],
            "test_dict": {
                "test": replacement_value,
                "test_list": [replacement_value, replacement_value],
                "test_dict_dict": {
                    "test": replacement_value,
                    "test_list": [replacement_value, replacement_value],
                },
            },
        }

        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_replace_string_in_dict_env.yaml"
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        logger.debug(global_config.string_replacement_map)
        assert global_config.EXAMPLE == global_config.dict_parameters["test"]
        assert global_config.dict_parameters == expected_dict

        os.environ.clear()
        os.environ.update(old_env)

    def test_build_config_mutual_exclusive_valid(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_mutual-exclusive_valid.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )

        logger.debug(configuration)

    def test_build_config_mutual_exclusive_failed(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_mutual-exclusive_invalid.yaml",
        )

        with raises(ValueError) as ve:
            configuration: GlobalConfig = typing.cast(
                GlobalConfig,
                ConfigBuilder(
                    class_type=GlobalConfig,
                    yaml_config_path=config_path,
                ).configuration,
            )

            logger.debug(configuration)

            assert (
                str(ve)
                == "mutual exclusive validated for class 'GlobalConfigCheckMutual', "
                "found attributes: ['mutual_1', 'mutual_2']"
            )

    def test_build_config_string_replacement(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_string_replacement.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map={"EXAMPLE": "TEST", "EMPTY_KEY": ""},
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert configuration.dict_parameters["TEST"] == ["EMPTY_KEY"]

    def test_build_config_string_replacement_after_build(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_string_replacement.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )
        configuration.recursive_string_replacement(
            string_replacement_map={"EXAMPLE": "TEST", "EMPTY_KEY": ""}
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert configuration.dict_parameters["TEST"] == ["EMPTY_KEY"]

    def test_build_config_string_replacement_in_sequence(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_string_replacement_in_sequence.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.sequence[0].seq_1 == "seq_1"
        assert configuration.sequence[0].seq_2 == "seq_2"
        assert configuration.sequence[1].seq_1 == "seq_3"
        assert configuration.sequence[1].seq_2 == "seq_4"

    def test_build_config_path_replacement(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_path_replacement.yaml",
        )
        replacement_map["EXAMPLE"] = ""

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE

    def test_build_config_path_replacement_with_os_env(self) -> None:
        project_root_dir, _ = get_project_root()
        config_path = (
            "${PROJECT_ROOT_DIR}/"
            "tests/data/test_build_config_path_replacement_with_os_env.yaml"
        )
        old_env = copy.deepcopy(os.environ)

        os.environ["PROJECT_ROOT_DIR"] = project_root_dir
        os.environ["TEST_BUILD_CONFIG"] = "EXAMPLE"

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE == "EXAMPLE"
        assert configuration.EXAMPLE_REPLACEMENT == "EXAMPLE_NO_REPLACEMENT"
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == "EXAMPLE"

        os.environ = old_env

    def test_build_config_path_replacement_in_yaml_path(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = "PROJECT_ROOT_DIR/tests/data/test_config_base.yaml"

        replacement_map["PROJECT_ROOT_DIR"] = project_root_dir

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        assert configuration.EXAMPLE == "TEST_BUILD_CONFIG"
        assert configuration.EXAMPLE_REPLACEMENT == "EXAMPLE_STRING"

    def test_build_config_path_replacement_in_yaml_path_not_valid(self) -> None:
        config_path = "PROJECT_ROOT_DIR/tests/data/test_config_base.yaml"

        with raises(ValueError) as ve:
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map={},
            )

            assert (
                str(ve) == "Given config-path does not exist: "
                "PROJECT_ROOT_DIR/tests/data/test_config_base.yaml"
            )

    def test_build_config_join_object(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_join_object.yaml"
        )

        os_value = os.getenv(key="EXAMPLE")
        os.environ.setdefault(key="EXAMPLE", value="TEST_EXAMPLE_FROM_OS")

        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )
        if os_value is None:
            os.environ.pop("EXAMPLE")
        else:
            os.environ["EXAMPLE"] = os_value

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_object_relative_dir(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_join_object__relative-dir.yaml"
        )

        os_value = os.getenv(key="EXAMPLE")
        os.environ.setdefault(key="EXAMPLE", value="TEST_EXAMPLE_FROM_OS")

        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )
        if os_value is None:
            os.environ.pop("EXAMPLE")
        else:
            os.environ["EXAMPLE"] = os_value

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_object_multiple(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir, "tests/data/test_config_join_object_multiple.yaml"
        )

        os_value = os.getenv(key="EXAMPLE")
        os.environ.setdefault(key="EXAMPLE", value="TEST_EXAMPLE_FROM_OS")

        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )
        if os_value is None:
            os.environ.pop("EXAMPLE")
        else:
            os.environ["EXAMPLE"] = os_value

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 4

    def test_build_config_join_object_valueerror(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_valueerror.yaml",
        )

        with raises(ValueError):
            _ = ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

    def test_build_config_join_object_real_path(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_real_path.yaml",
        )

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map={
                    "EXAMPLE": "",
                    "PROJECT_ROOT_DIR": project_root_dir,
                },
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_object_uft8(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_uft8_üäö.yaml",
        )
        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == "TEST"
        assert configuration.EXAMPLE_LIST_REPLACEMENT[1] == "TEST-2-üäö"
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_object_without_key(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_without_key.yaml",
        )
        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_from_config_dir(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_from_config_dir.yaml",
        )
        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_from_config_dir_multiple_dirs(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/" "test_config_join_object_from_config_dir_multiple_dirs.yaml",
        )
        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_config_join_from_config_dir_filenotfounderror(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            (
                "tests/data/"
                "test_config_join_object_from_config_dir_filenotfounderror.yaml"
            ),
        )

        with raises(FileNotFoundError):
            _ = ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

    def test_build_config_join_from_config_dir_valueerror(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/" "test_config_join_object_from_config_dir_valueerror.yaml",
        )

        with raises(ValueError):
            _ = ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
            )

    def test_build_config_join_from_config_dir_empty_key(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_from_config_dir_empty_key.yaml",
        )
        replacement_map["EXAMPLE"] = ""
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        # Check that value has been replaced correctly
        assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
        assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
        assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_build_super_class_with_argparse(self) -> None:
        project_root_dir, _ = get_project_root()

        class SubConfig(ConfigBuilder):
            def __init__(self) -> None:
                ConfigBuilder.__init__(
                    self,
                    class_type=GlobalConfig,
                    string_replacement_map={"PROJECT_ROOT_DIR": ""},
                    use_argparse=True,
                )

                self.configuration: GlobalConfig = typing.cast(
                    GlobalConfig, self.configuration
                )

        if len(sys.argv) == 2:
            sys.argv = sys.argv[1:]

        sys.argv.clear()

        sys.argv.append(os.path.join(project_root_dir, "tests"))

        sys.argv.extend(
            [
                os.path.join(
                    project_root_dir,
                    "tests/data/test_config_super_class_with_argparse.yaml",
                ),
            ]
        )
        sys.argv.extend(
            [
                "--replacement-config-path",
                os.path.join(
                    project_root_dir,
                    "tests/data/test_replacement_map.yaml",
                ),
            ]
        )
        sys.argv.extend(["--PROJECT_ROOT_DIR", project_root_dir])

        logger.debug("\n\n==============================")
        logger.debug(sys.argv)
        logger.debug("==============================\n\n")

        sub_config = SubConfig()

        logger.debug(sub_config.configuration)

        sub_config.configuration = typing.cast(GlobalConfig, sub_config.configuration)

        # Check that value has been replaced correctly
        assert sub_config.configuration.EXAMPLE_REPLACEMENT == "ARGPARSE_REPLACEMENT"
        # Check that value has been replaced correctly
        assert sub_config.configuration.EXAMPLE == "EXAMPLE"

    def test_build_super_class_with_argparse_no_replacement_config(self) -> None:
        project_root_dir, _ = get_project_root()

        class SubConfig(ConfigBuilder):
            def __init__(self) -> None:
                ConfigBuilder.__init__(
                    self,
                    class_type=GlobalConfig,
                    string_replacement_map={"_PROJECT_ROOT_DIR_": ""},
                    use_argparse=True,
                )

        if len(sys.argv) == 2:
            sys.argv = sys.argv[1:]

        sys.argv.clear()

        sys.argv.append(os.path.join(project_root_dir, "tests"))

        sys.argv.extend(
            [
                os.path.join(
                    project_root_dir,
                    "tests/data/test_config_super_class_with_argparse.yaml",
                ),
            ]
        )
        sys.argv.extend(
            [
                "--replacement-config-path",
                "INVALID_PATH",
            ]
        )

        logger.debug("\n\n==============================")
        logger.debug(sys.argv)
        logger.debug("==============================\n\n")

        sub_config = SubConfig()

        logger.debug(sub_config.configuration)

        sub_config.configuration = typing.cast(GlobalConfig, sub_config.configuration)

        # Check that value has been replaced correctly
        assert sub_config.configuration.EXAMPLE_REPLACEMENT == "ARGPARSE_PLACEHOLDER"
        # Check that value has been replaced correctly
        assert sub_config.configuration.EXAMPLE == "EXAMPLE"

    def test_build_super_class_with_additional_argparse(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        replacement_map["EXAMPLE"] = ""

        class SubConfig(ConfigBuilder):
            def __init__(self) -> None:
                ConfigBuilder.__init__(
                    self,
                    class_type=GlobalConfig,
                    string_replacement_map=replacement_map,
                    configure_argparse=(
                        lambda parser: parser.add_argument(
                            "--test", help="Test to add argument", type=str
                        )
                    ),
                    use_argparse=True,
                )

                self.configuration: GlobalConfig = typing.cast(
                    GlobalConfig, self.configuration
                )

        if len(sys.argv) == 2:
            sys.argv = sys.argv[1:]

        sys.argv.clear()

        sys.argv.append(os.path.join(project_root_dir, "tests"))

        sys.argv.extend(
            [
                os.path.join(
                    project_root_dir,
                    "tests/data/test_config_super_class_with_additional_argparse.yaml",
                ),
            ]
        )
        sys.argv.extend(["--PROJECT_ROOT_DIR", project_root_dir])
        sys.argv.extend(["--test", "test"])

        logger.debug("\n\n==============================")
        logger.debug(sys.argv)
        logger.debug("==============================\n\n")

        sub_config = SubConfig()

        logger.debug(sub_config.configuration)

        sub_config.configuration = typing.cast(GlobalConfig, sub_config.configuration)

        # Check that value has been replaced correctly
        assert (
            sub_config.configuration.EXAMPLE_REPLACEMENT
            == sub_config.configuration.EXAMPLE
        )
        assert (
            sub_config.configuration.EXAMPLE_LIST_REPLACEMENT[0]
            == sub_config.configuration.EXAMPLE
        )

    def test_build_super_class_with_additional_argparse_file_not_found(self) -> None:
        project_root_dir, replacement_map = get_project_root()

        class SubConfig(ConfigBuilder):
            def __init__(self) -> None:
                ConfigBuilder.__init__(
                    self,
                    class_type=GlobalConfig,
                    configure_argparse=(
                        lambda parser: parser.add_argument(
                            "--test", help="Test to add argument", type=str
                        )
                    ),
                    use_argparse=True,
                )

                self.yaml_config = typing.cast(SubConfig, self.configuration)

        if len(sys.argv) == 2:
            sys.argv = sys.argv[1:]

        sys.argv.clear()

        sys.argv.append(os.path.join(project_root_dir, "tests"))

        sys.argv.extend(
            [
                os.path.join(
                    project_root_dir,
                    "tests/data/test_config_super_class_with_additional_argparse.yaml",
                ),
            ]
        )
        sys.argv.extend(["--test", "test"])

        logger.debug(
            "\n\n==============================\n"
            f"{sys.argv}\n"
            "==============================\n\n"
        )

        with pytest.raises(FileNotFoundError):
            sub_config = SubConfig()

            logger.debug(sub_config.configuration)

            sub_config.configuration = typing.cast(
                GlobalConfig, sub_config.configuration
            )

    def test_build_config_with_os_environment_replacements(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_with_os_environment.yaml",
        )
        replacement_map["EXAMPLE"] = ""

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        # Check that value has been replaced correctly
        assert global_config.EXAMPLE_REPLACEMENT == global_config.EXAMPLE
        assert global_config.EXAMPLE_LIST_REPLACEMENT[0] == global_config.EXAMPLE

    def test_build_config_with_external_os_environment_replacements(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_join_object_with_os_environment.yaml",
        )
        replacement_map["EXAMPLE"] = ""

        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        logger.debug(configuration)

        global_config = configuration

        # Check that value has been replaced correctly
        assert global_config.EXAMPLE_REPLACEMENT == global_config.EXAMPLE
        assert global_config.EXAMPLE_LIST_REPLACEMENT[0] == global_config.EXAMPLE

    def test_build_config_allow_missing_path_replacement(self) -> None:
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(
            project_root_dir,
            "tests/data/test_config_allow_missing_path_replacement.yaml",
        )

        check_mutual_config: Optional[GlobalConfigCheckMutual] = None

        with open(file=config_path, mode="r") as config_file:
            yaml_dict = yaml.load(stream=config_file, Loader=FullLoader)

        try:
            check_mutual_config_dict = yaml_dict.pop("mutual_exclusive")

            if check_mutual_config_dict is not None:
                check_mutual_config = to_model(
                    GlobalConfigCheckMutual, check_mutual_config_dict
                )
        except ValueError:
            pass
        except KeyError:
            pass

        logger.debug(check_mutual_config)

    def test_build_config_join_object_different_project_root_dirs(self) -> None:
        project_root_dir, _ = get_project_root()

        config_path_1 = os.path.join(
            project_root_dir,
            "tests/data/project_root_dir_1/test_config_join_object.yaml",
        )
        project_root_dir_1 = os.path.join(
            project_root_dir, "tests/data/project_root_dir_1"
        )

        config_path_2 = os.path.join(
            project_root_dir,
            "tests/data/project_root_dir_2/test_config_join_object.yaml",
        )
        project_root_dir_2 = os.path.join(
            project_root_dir, "tests/data/project_root_dir_2"
        )

        config_tuples = [
            (config_path_1, project_root_dir_1),
            (config_path_2, project_root_dir_2),
        ]

        for config_tuple in config_tuples:
            config_path, cur_project_root_dir = config_tuple

            configuration: GlobalConfig = typing.cast(
                GlobalConfig,
                ConfigBuilder(
                    class_type=GlobalConfig,
                    yaml_config_path=config_path,
                    string_replacement_map={
                        "PROJECT_ROOT_DIR_2": cur_project_root_dir,
                        "EXAMPLE": "",
                    },
                ).configuration,
            )

            logger.debug(configuration)

            # Check that value has been replaced correctly
            assert configuration.EXAMPLE_REPLACEMENT == configuration.EXAMPLE
            assert configuration.EXAMPLE_LIST_REPLACEMENT[0] == configuration.EXAMPLE
            assert len(configuration.EXAMPLE_LIST_REPLACEMENT) == 2

    def test_replacement_map_correctly_set(self) -> None:
        clear_replacement_map()
        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(project_root_dir, "tests/data/test_config_base.yaml")

        # We want that the content of the attribute EXAMPLE is used to fill the replacement map
        # Therefore set the value initially to 'TO_BE_REPLACED' and check that the value gets
        # filled correctly.
        replacement_map = {"EXAMPLE": "TO_BE_REPLACED"}
        expected_replacement_map = {"EXAMPLE": "TEST_BUILD_CONFIG"}
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        global_replacement_map = get_current_replacement_map()

        logger.debug(configuration)

        assert global_replacement_map == expected_replacement_map

    def test_replacement_map_correctly_set_by_os(self) -> None:
        clear_replacement_map()
        expected_replacement_map = {
            "CONFIG_BUILDER_TEST_EXAMPLE": "EXAMPLE_OVERWRITTEN",
        }

        os.environ["CONFIG_BUILDER_TEST_EXAMPLE"] = "EXAMPLE_OVERWRITTEN"

        set_replacement_map_value(key="CONFIG_BUILDER_TEST_EXAMPLE", value="EXAMPLE")
        update_replacement_map_from_os()

        project_root_dir, replacement_map = get_project_root()
        config_path = os.path.join(project_root_dir, "tests/data/test_config_base.yaml")

        replacement_map = get_current_replacement_map()
        configuration: GlobalConfig = typing.cast(
            GlobalConfig,
            ConfigBuilder(
                class_type=GlobalConfig,
                yaml_config_path=config_path,
                string_replacement_map=replacement_map,
            ).configuration,
        )

        global_replacement_map = get_current_replacement_map()

        logger.debug(configuration)

        assert global_replacement_map == expected_replacement_map

    def test_build_config_thread_safe(self) -> None:
        thread_count = 50

        global_results: List[Optional[str]] = [None] * thread_count

        def __test_build_config_base(
            results: List[Optional[str]],
            index: int,
            string_replacement_map: Optional[Dict[str, str]] = None,
        ) -> None:
            project_root_dir, replacement_map = get_project_root()
            config_path = os.path.join(
                project_root_dir, "tests/data/test_config_base.yaml"
            )

            configuration: GlobalConfig = typing.cast(
                GlobalConfig,
                ConfigBuilder(
                    class_type=GlobalConfig,
                    yaml_config_path=config_path,
                    string_replacement_map=string_replacement_map,
                ).configuration,
            )

            configuration = typing.cast(GlobalConfig, configuration)

            global_config = configuration

            results[index] = global_config.string_replacement_map["EXAMPLE_STRING"]

        thread_list: List[Thread] = []

        for i in range(0, thread_count):
            # create two new threads
            thread_list.append(
                Thread(
                    target=__test_build_config_base,
                    args=(global_results, i, {"EXAMPLE_STRING": f"{i}"}),
                )
            )

        for thread in thread_list:
            # start the threads
            thread.start()

        for thread in thread_list:
            # wait for the threads to complete
            thread.join()

        if len(set(global_results)) != len(global_results):
            for result in global_results:
                print(f"replacement map: {result}")

            raise ValueError("Concurrent modification of global replacement map!")
