from __future__ import annotations

import logging
from typing import Any, Dict, List, Optional

import related
from attr import define

from config_builder import BaseConfigClass

logger = logging.getLogger(__name__)


@define
class GlobalConfigCheckMutualNested(BaseConfigClass):
    nested: str = related.StringField(required=False, default="")

    def check_values(self) -> bool:
        return self.nested != "FORBIDDEN"


@define
class GlobalConfigSequenceAttribute(BaseConfigClass):
    seq_1: str = related.StringField()
    seq_2: str = related.StringField()

    nested_attribute: GlobalConfigCheckMutualNested = related.ChildField(
        cls=GlobalConfigCheckMutualNested,
        required=False,
        default=GlobalConfigCheckMutualNested(),
    )

    def check_values(self) -> bool:
        return self.seq_1 != "FORBIDDEN" and self.seq_2 != "FORBIDDEN"


@define
class GlobalConfigCheckMutual(BaseConfigClass):
    mutual_1: Optional[int] = related.IntegerField(required=False, default=None)
    mutual_2: Optional[int] = related.IntegerField(required=False, default=None)

    nested_attribute: GlobalConfigCheckMutualNested = related.ChildField(
        cls=GlobalConfigCheckMutualNested,
        required=False,
        default=GlobalConfigCheckMutualNested(),
    )

    @property
    def _mutual_attributes(self) -> List[str]:
        return ["mutual_1", "mutual_2"]

    def check_values(self) -> bool:
        if self.mutual_1 is not None:
            return self.mutual_1 >= 0
        else:
            return True


@define
class GlobalConfig(BaseConfigClass):
    EXAMPLE: str = related.StringField(required=False, default="EXAMPLE_REPLACEMENT")
    EXAMPLE_REPLACEMENT: str = related.StringField(
        required=False, default="EXAMPLE_STRING"
    )

    EXAMPLE_LIST_REPLACEMENT: List[str] = related.SequenceField(
        cls=str, required=False, default=list()
    )

    dict_parameters: Optional[Dict[str, Any]] = related.ChildField(
        cls=dict, default=None, required=False
    )

    mutual_exclusive: GlobalConfigCheckMutual = related.ChildField(
        cls=GlobalConfigCheckMutual, required=False, default=None
    )

    sequence: List[GlobalConfigSequenceAttribute] = related.SequenceField(
        cls=GlobalConfigSequenceAttribute, required=False, default=None
    )

    def check_values(self) -> bool:
        return self.EXAMPLE != "FORBIDDEN"


@define
class GlobalConfig2(BaseConfigClass):
    __related_strict__ = True

    test_attribute = related.StringField()
