import json
from collections import OrderedDict

from config_builder.json_encoding import TupleEncoder


def test_json_encoding_json_decoding() -> None:
    test_dict = {
        "integer": 1,
        "dict": OrderedDict({"a": 1, "b": 2, "test_tuple": (1, 2)}),
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "test_tuple": (1, 2),
        "test_list": [1, 1],
        "test_tuple_list": [(1, 2), (1, 2), (1, 2)],
    }

    expected_dict = {
        "integer": 1,
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "test_tuple": (1, 2),
        "test_list": [1, 1],
        "test_tuple_list": [(1, 2), (1, 2), (1, 2)],
    }

    encoder = TupleEncoder()
    json_string = encoder.encode(test_dict)

    converted_dict = json.loads(
        json_string, object_hook=TupleEncoder.tuple_save_loading_hook
    )

    assert converted_dict == expected_dict


def test_json_encoding_dict_decoding() -> None:
    test_dict = {
        "integer": 1,
        "dict": OrderedDict({"a": 1, "b": 2, "test_tuple": (1, 2)}),
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "test_tuple": (1, 2),
        "test_list": [1, 1],
        "test_tuple_list": [(1, 2), (1, 2), (1, 2)],
    }

    expected_dict = {
        "integer": 1,
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "dict": {"a": 1, "b": 2, "test_tuple": (1, 2)},
        "test_tuple": (1, 2),
        "test_list": [1, 1],
        "test_tuple_list": [(1, 2), (1, 2), (1, 2)],
    }

    encoder = TupleEncoder()
    json_string = encoder.encode(test_dict)

    converted_dict = TupleEncoder.decode(json.loads(json_string))

    assert converted_dict == expected_dict
