# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Define common fixtures for the unit tests."""

import os
from logging import getLogger
from pathlib import Path

import pytest

logger = getLogger(__name__)


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path."""

    return Path(os.path.dirname(os.path.abspath(__file__))).resolve().parent
