# Yaml-Config-Builder Versions:

8.6.0 (2025-01-09):
------------------
Add utility methods to generate flask-restx schema models from attr classes

8.5.0 (2024-10-21):
------------------
Add method for build configuration from dict including the necessary pre- and post-processing

8.4.0 (2024-10-21):
------------------
Allow the pattern ${...} to be used for string replacement with os environment variables

8.3.0 (2024-02-20):
------------------
Add TupleEncoder.decode to allow the decoding of dictionaries

8.2.0 (2023-07-25):
------------------
Add TupleEncoder for tuple-save encoding and decoding of dictionaries to json and vice versa

8.1.1 (2023-06-22):
------------------
Fix the string replacement in windows style paths

8.1.0 (2023-05-31):
------------------
Recursively resolve replacements in dicts

8.0.2 (2023-05-05):
------------------
Relicense to OLFL-1.3 which succeeds the previous license

8.0.1 (2023-05-05):
------------------
Allow to use relative path like ".." in combination with string replacement

8.0.0 (2023-02-06):
------------------
Stabilize behavior between related and the attrs package
- Don't make use of the related decorators but assume that every
  configuration class uses the @define decorator of the attrs package
- Change the BaseConfigClass attribute 'mutual_attribute_map' to the
  protected attribute '_mutual_attributes'. Herewith every class has
  as a static definition of its mutual attributes.
- Change the string-replacement-map to be private and only accessible
  via a property. This solves the problem of unexpected key-word
  arguments when converting config classes. Furthermore, the
  __attrs_post_init__ is used to avoid the wrong order of default
  arguments in subclasses

7.1.1 (2023-02-03):
------------------
Update dependency version ranges to allow newer dependencies

7.1.0 (2022-12-14):
------------------
Allow multiple entries for !join_object

7.0.0 (2022-08-30):
------------------
Stabilize configuration build behavior
- Remove passthrough of an already built configuration object
- Make argparse argument "yaml_config_path" non-optional
- New API for passing argparse configuration

6.0.0 (2022-06-29):
------------------
Rename package from config-builder to yaml-config-builder

# Config-Builder Versions:

6.0.0 (2022-06-21):
------------------
Enhance the handling of commandline parameters:
- Don't use None values of commandline arguments for replacement overwrites
- Add '--replacement-config-path' argparse option
- Allow to disable the fallback to commandline parameters

5.0.1 (2022-03-28):
------------------
- Fix update of the yaml_config_path attribute of the ConfigBuilder

5.0.0 (2022-02-24):
------------------
Remove static placeholders:
- With the changes made in version 3.0.0 it is no longer needed to
  have a dedicated handling of the CONFIG_ROOT_DIR placeholder
- Remove the occurrences of the CONFIG_ROOT_DIR and the associated parameters
Refactor the build of a configuration object:
- Restructure methods and encapsulate functionality in less cognitive complex manner
- Split up the functionality of recursive_check_and_update

4.0.1 (2022-02-23):
------------------
Fix minor typing issues in YAML constructor potentially
leading to typing issues downstream

4.0.0 (2022-02-22):
------------------
Rename ConfigBuilder attribute from base_config to configuration:
- The name "base_config" is misleading since there are no other
  configurations and therefore there is no "base" configuration
- Use more appropriate name "configuration" instead
- Fix unit tests to be able to be executed from any working directory

3.1.0 (2022-02-02):
------------------
Don't touch logging configuration inside library code

3.0.0 (2022-01-18):
------------------
Fix issues and refactor the behavior of the string replacement:
- Combine the behavior of the string replacement map and the os string replacement map
- Use one central string replacement map as constructor parameter of the ConfigBuilder
- The string replacement map is handled as a threading.local object
- Remove the usage of the string replacement map that is defined per BaseConfigClass instance
  and its derivatives
- Order of precedence for placeholder values (defined in the constructor of the ConfigBuilder):
  1. the values of set by OS environment variables
  2. the values given by commandline arguments (if the ConfigBuilder is used in that regard)
  3. the values given by the constructor parameter
  4. config files

2.4.1 (2021-12-16):
------------------
- Correct settings for mypy and pylint
- Fix arising typing errors
- add isort gitlab-ci stage
- add docstrings and refactor error handling / error messages

2.4.0 (2021-12-15):
------------------
- Add OLF License
- Add method to ensure that only the exact matching strings are replaced in a path

2.3.3 (2021-12-14):
------------------
- Minor refactorings
- Update related-mltoolbox

2.3.2 (2021-12-10):
------------------
- Ensure that the config-root-dir has been set correctly
- When no valid config-root-dir is given use the value of the os environment variable

2.3.1 (2021-12-08):
------------------
- Ensure correct check for overwriting values from os variable content

2.3.0 (2021-12-08):
------------------
- Remove usage of setting os environment variables
  - This has let to unwanted side effects and is an unstable feature
  - As alternative a dedicated replacement dictionary is used
  - It is private variable which can only be accessed via dedicated getter and setter

2.2.1 (2021-11-17):
------------------
- change build from pip+setup.py to poetry

2.2.0 (2021-11-10):
------------------
- add string replacement for attributes of type dict

2.1.0 (2021-08-23):
------------------
- ensure correct behavior of custom yaml constructors
- move some methods to avoid circular imports
- remove OrderedLoader class

2.0.0 (2021-08-12):
------------------
- refactor the whole config-builder
- use related as dependency and not in the project itself
- add a gitlab-runner pipeline

1.8.0 (2021-08-09):
------------------
- source out methods to different files
- add a central file to define constants
- reuse code provided in utils methods
- ensure that os-variables can be overwritten. This is needed to allow
  that a config object can be build with different environment settings.
  Use case is e.g. a pipeline where different config objects have to
  be build one after another
- add a dedicated test case to ensure that different os-variables can be used

1.7.0 (2021-06-23):
------------------
- use uft8 encoding to load configuration files
- don't trigger FileNotFoundException in yaml constructors, but
  add functionality to detect them later on

1.6.2 (2021-06-18):
------------------
- fix failure on join_object_from_config_dir when using multiple directories

1.6.1 (2021-06-18):
------------------
- adapt logging messages to be more clear to state what appends and why something fails

1.6.0 (2021-05-20):
------------------
- separate dependencies into runtime and develop
- ensure that yaml-config-path exists
- ensure that the os-string-replacement map is set before using it
- replace print by logging mechanism

1.5.0 (2021-03-18):
------------------
- when using custom yaml constructors in
  src/config_builder/related/functions.py it is now possible to include
  placeholders which are specified via the os_string_replacement_map

- these placeholders can either be parsed from commandline, directly
  set in the code, or specified in the terminal environment before
  executing the code

1.4.2 (2021-03-17):
------------------
- ensure that the string-replacement of multiple placeholders
  in a given string is executed correctly

1.4.1 (2021-03-15):
------------------
- refactor parsing via !join_object and !join_object_from_config_dir
  - add joined methods for overlapping functionality
  - allow handing over an emtpy string as "class-type". This indicates,
  that the whole content of the included yaml file should be used to
  build the config-object which is indicated with !join_object or !join_from_config_dir

- add getter for the environment variable CONFIG_ROOT_DIR

1.4.0 (2021-03-02):
------------------
- add the possibility to use the os environment variable 'CONFIG_ROOT_DIR' to be used as placeholder in config files
  - the placeholder defines the root-directory for given config-files
  - in conjunction with relative paths it allows defining config-files,
  which are valid across multiple devices

1.3.4 (2021-02-25)
------------------
- adapt the way how the default argument "yaml-config-path" is set

1.3.3 (2021-02-22)
------------------
- fix "/" in setup.py, to support windows installs
- correct type-hint for mutual_attribute_map


1.3.2 (2021-02-05)
------------------
- make _add_argparse_parameters protected instead of private, separate instantiation from building with additional run_build method
- add test

1.3.1 (2021-01-06)
------------------
- Add check_mutual_attributes_flag and check_asserts_flag to constructor of the ConfigBuilder class.
  Use the parameter in the build_config method appropriately.

1.3.0 (2021-01-06)
------------------
- the primary method for parsing is "parse_arguments"
- __setup_argparse is used setting an ArgumentParser instance
- __add_argparse_parameters should be used to define additional arguments

1.2.0 (2021-01-06)
------------------
- replace the "cmp" parameter by "eq" in the attrib constructor in related/fields.py

1.1.1 (2021-01-06)
------------------
- add method 'to_python_dict' to __init__ so that it is available for api usage
- add test for that

1.1.0 (2021-01-05)
------------------
- add 'mutual_attribute_map' as parameter for the ConfigBuilder in the constructor and build_config
- add test cases for mutual attributes and string replacements
- add basics for running mypy

1.0.0 (2020-12-07)
------------------
- Wrap related package
- Introduce BaseConfigClass and ConfigBuilder as starting point for new configurations
  of executables
